const {src, dest, parallel, watch} = require('gulp');
const browserSync = require('browser-sync').create();

function launchBrowserSync() {
	browserSync.init({
		server: {baseDir: 'public/' },
		notify: false,
		online: true
	})
}

function startWatch() {
	watch('public/**').on('change', browserSync.reload);
}

function startProject() {
	launchBrowserSync();
	startWatch();
}



exports.bs = launchBrowserSync;
exports.watch = startWatch;
exports.start = startProject;